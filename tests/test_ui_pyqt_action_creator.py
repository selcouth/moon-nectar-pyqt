import pytest

from mnectar.action import Action, Actionable
from mnectar.config import Setting
from mnectar.util.signal import Signal
from mnpyqt.core.ActionCreator import ActionCreator


class TestActionCreator:
    def test_actionable(self, logconfig, app, qtbot, main):
        logconfig({"mnectar.action": "DEBUG3"}, default="DEBUG")

        class ActionTester(Actionable):
            # fmt: off
            set_bool = Setting('setting.is_bool', default=False)
            set_str  = Setting('setting.is_str', default='val1')
            sig_1    = Signal()
            sig_2    = Signal()
            sig_3    = Signal()

            act_bool = Action("Menu1",         "", "SettingBool", setting=set_bool)
            act_str1 = Action("Menu1",         "", "SettingStr",  setting=set_str, args=("val1", ))
            act_str2 = Action("Menu1",         "", "SettingStr",  setting=set_str, args=("val2", ))
            act_sig1 = Action("Menu1|Submenu", "", "Signal_1",    signal=sig_1)
            act_sig3 = Action("Menu1|Submenu", "", "Signal_3",    signal=sig_3,    args=(True,   True))

            sig_1_v  = False
            sig_2_v  = False
            sig_3_v  = (False, False)
            # fmt: on

            @act_bool.triggered
            def on_set_bool(self, state):
                assert self.set_bool == state

            @act_str1.triggered
            def on_set_str1(self, value):
                assert value == "val1"

            @act_str2.triggered
            def on_set_str2(self, value):
                assert value == "val2"

            @act_sig1.triggered
            def on_sig1(self):
                self.sig_1_v = True

            @act_sig3.triggered
            def on_sig3(self, val1, val2):
                self.sig_3_v = (val1, val2)

        act = ActionTester(app=app)
        obj = ActionCreator(app=app)
        obj.create_menu_actions(main.menubar)
