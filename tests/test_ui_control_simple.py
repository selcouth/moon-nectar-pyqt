import copy
import logging
import pytest
import time

from PyQt6 import QtCore

from mnpyqt.core.PlaybackControl import PlaybackControl
from mnectar.library.view import ViewPointer


def setup_module(module):
    module.logger = logging.getLogger("mnpyqt.core.PlaybackControl")
    module.logger_old_level = logger.level
    module.logger.setLevel("DEBUG3")
    logging.getLogger("mnectar.util.signal").setLevel("DEBUG3")


def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)


@pytest.fixture
def controller(qtbot, main):
    qtbot.addWidget(main)
    main.show()
    control = PlaybackControl(main)
    controldock = control.make_dock_widget(main)
    qtbot.waitExposed(controldock)

    qtbot.addWidget(control)

    yield main, control


def test_control_play_next(qtbot, controller):
    main, control = controller

    with qtbot.waitSignal(main.app.signal.playNext, timeout=1000) as sig:
        qtbot.mouseClick(control.ui.Next, QtCore.Qt.LeftButton)


def test_control_play_prev(qtbot, controller):
    main, control = controller

    with qtbot.waitSignal(main.app.signal.playPrev, timeout=1000) as sig:
        qtbot.mouseClick(control.ui.Prev, QtCore.Qt.LeftButton)


def test_control_stop(qtbot, controller, mocker):
    main, control = controller

    with qtbot.waitSignal(main.app.signal.stop, timeout=1000) as sig:
        qtbot.mouseClick(control.ui.Stop, QtCore.Qt.LeftButton)


def test_control_play_pause(qtbot, controller):
    main, control = controller

    with qtbot.waitSignal(main.app.signal.play, timeout=1000) as sig:
        qtbot.mouseClick(control.ui.PlayPause, QtCore.Qt.LeftButton)

    assert control.ui.PlayPause.isChecked()

    with qtbot.waitSignal(main.app.signal.pause, timeout=1000) as sig:
        qtbot.mouseClick(control.ui.PlayPause, QtCore.Qt.LeftButton)

    assert not control.ui.PlayPause.isChecked()


def test_control_play_next_ext(qtbot, controller):
    main, control = controller

    def assert_down():
        assert control.ui.Next.isDown()
    def assert_up():
        assert not control.ui.Next.isDown()

    main.app.signal.mmkey_playNext.emit()

    qtbot.waitUntil(assert_down)
    qtbot.waitUntil(assert_up)


def test_control_play_prev_ext(qtbot, controller):
    main, control = controller

    def assert_down():
        assert control.ui.Prev.isDown()
    def assert_up():
        assert not control.ui.Prev.isDown()

    main.app.signal.mmkey_playPrev.emit()

    qtbot.waitUntil(assert_down)
    qtbot.waitUntil(assert_up)


def test_control_mmkey(qtbot, controller):
    main, control = controller

    def assert_down():
        assert control.ui.PlayPause.isDown()
    def assert_up():
        assert not control.ui.PlayPause.isDown()

    def assert_not_playing():
        assert not control.ui.PlayPause.isChecked()
    def assert_playing():
        assert control.ui.PlayPause.isChecked()

    with qtbot.waitSignal(main.app.signal.play, timeout=1000) as sig:
        qtbot.mouseClick(control.ui.PlayPause, QtCore.Qt.LeftButton)
    qtbot.waitUntil(assert_playing)

    main.app.signal.mmkey_togglePause.emit()
    qtbot.waitUntil(assert_down)
    qtbot.waitUntil(assert_up)
    qtbot.waitUntil(assert_not_playing)

    main.app.signal.mmkey_togglePause.emit()
    qtbot.waitUntil(assert_down)
    qtbot.waitUntil(assert_up)
    qtbot.waitUntil(assert_playing)

def test_control_exeternal_play_pause_stop(qtbot, controller):
    main, control = controller

    def assert_down():
        assert control.ui.PlayPause.isDown()
    def assert_up():
        assert not control.ui.PlayPause.isDown()

    def assert_not_playing():
        assert not control.ui.PlayPause.isChecked()
    def assert_playing():
        assert control.ui.PlayPause.isChecked()

    pointer = ViewPointer(app=main.app)
    main.app.signal.playing.emit(pointer,0)
    qtbot.waitUntil(assert_playing)

    main.app.signal.paused.emit()
    qtbot.waitUntil(assert_not_playing)

    main.app.signal.stopped.emit()
    qtbot.waitUntil(assert_not_playing)

