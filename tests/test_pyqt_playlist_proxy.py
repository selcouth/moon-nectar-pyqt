import os
import pytest
import pathlib

from collections import Counter
from PyQt6 import QtCore
from PyQt6 import QtGui
from pyfakefs.fake_filesystem_unittest import Patcher as FsFakePatcher

from mnectar.library.view import Sorted, View
from mnpyqt.core import QPlaylistModel
from mnpyqt.core import QPlaylistFilterSortProxy


def assert_index_eq(index1, index2):
    assert index1.row() == index2.row()
    assert index1.column() == index2.column()
    assert index1.model().view[index1.row()] == index2.model().view[index2.row()]


def assert_album_sort(check_view, ascending=True):
    if ascending:
        view = list(check_view)
    else:
        view = list(reversed(check_view))

    for row in range(1,len(view)):
        assert view[row]['album'] >= view[row-1]['album']
        if view[row]['album'] == view[row-1]['album']:
            assert view[row]['tracknumber'] > view[row-1]['tracknumber']


def test_pyqt_playlist_proxy_init(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    assert view in proxy.view.chain
    assert model.view in proxy.view.chain
    assert proxy.source_view == model.view
    assert proxy.app == app
    assert proxy.rowCount() == model.rowCount()
    assert proxy.columnCount() == model.columnCount()

def test_pyqt_playlist_proxy_init_error(qtbot, app, backend_mock, library_factory):
    with pytest.raises(ValueError):
        error_proxy = QPlaylistFilterSortProxy()
        error_proxy.setSourceModel([])


def test_pyqt_playlist_proxy_init_map(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    for row in range(len(view)):
        assert proxy.view[row] == model.view[row]

        for col in range(len(app.columns)):
            pidx = proxy.index(row, col)
            midx = model.index(row, col)

            assert_index_eq(pidx, midx)

            assert proxy.data(pidx, QtCore.Qt.DisplayRole) == model.data(midx, QtCore.Qt.DisplayRole)
            assert proxy.pointer(pidx).valid
            assert proxy.pointer(pidx) == proxy.view.pointer(row)
            assert proxy.pointer(pidx).record == view[row]
            assert not proxy.parent(pidx).isValid()

def test_pyqt_playlist_proxy_sorted_reverse(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = View(records, app)
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    # Verify the list does not start sorted
    # ... to ensure a valid test
    try:
        assert_album_sort(proxy.view)
    except AssertionError:
        pass
    else:
        assert False, "Invalid test: sorted same as original!"

    # Sort the list
    proxy.sort(app.columns.indexOfName('album'))

    # Verify the sorting is correct
    assert_album_sort(proxy.view)
    assert proxy.rowCount() == model.rowCount()

    # Reverse sort the list
    proxy.sort(app.columns.indexOfName('album'), order=QtCore.Qt.DescendingOrder)

    # Verify the sorting is correct
    assert_album_sort(proxy.view, ascending=False)
    assert proxy.rowCount() == model.rowCount()

    # Repeat the previous sort to verify default behavior
    proxy.sort()

    # Verify the sorting is correct
    assert_album_sort(proxy.view, ascending=False)
    assert proxy.rowCount() == model.rowCount()

def test_pyqt_playlist_proxy_sorted_map(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = View(records, app)
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    # Verify the list does not start sorted
    # ... to ensure a valid test
    try:
        assert_album_sort(proxy.view)
    except AssertionError:
        pass
    else:
        assert False, "Invalid test: sorted same as original!"

    # Sort the list
    proxy.sort(app.columns.indexOfName('album'))

    # Verify the sorting is correct (just in case)
    assert_album_sort(proxy.view)
    assert proxy.rowCount() == model.rowCount()

    # Verify proxy.mapToSource (proxy -> model)
    for row in range(proxy.rowCount()):
        for col in range(len(app.columns)):
            pidx = proxy.index(row, col)
            midx = proxy.mapToSource(pidx)
            mrow = midx.row()
            mcol = midx.column()

            assert pidx.model() == proxy
            assert midx.model() == model

            assert proxy.view[row] == model.view[mrow]

            assert proxy.data(pidx, QtCore.Qt.DisplayRole) == model.data(midx, QtCore.Qt.DisplayRole)
            assert proxy.data(pidx, QtCore.Qt.BackgroundRole) == model.data(midx, QtCore.Qt.BackgroundRole)

    # Verify proxy.mapFromSource (model -> proxy)
    for row in range(model.rowCount()):
        for col in range(len(app.columns)):
            midx = model.index(row, col)
            pidx = proxy.mapFromSource(midx)
            prow = pidx.row()
            pcol = pidx.column()

            assert pidx.model() == proxy
            assert midx.model() == model

            assert model.view[row] == proxy.view[prow]

            assert proxy.data(pidx, QtCore.Qt.DisplayRole) == model.data(midx, QtCore.Qt.DisplayRole)
            assert proxy.data(pidx, QtCore.Qt.BackgroundRole) == model.data(midx, QtCore.Qt.BackgroundRole)


def test_pyqt_playlist_proxy_filtered_map(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    # Gather the list of available albums
    albums = Counter(_['album'] for _ in view)

    for album in albums:
        proxy.filter(f"(album={album})")
        assert proxy.rowCount() == albums[album]

        # Verify proxy.mapToSource (proxy -> model)
        for row in range(proxy.rowCount()):
            assert proxy.view[row]['album'] == album
            for col in range(len(app.columns)):
                pidx = proxy.index(row, col)
                midx = proxy.mapToSource(pidx)
                mrow = midx.row()
                mcol = midx.column()

                assert pidx.model() == proxy
                assert midx.model() == model

                assert proxy.view[row] == model.view[mrow]

                assert proxy.data(pidx, QtCore.Qt.DisplayRole) == model.data(midx, QtCore.Qt.DisplayRole)
                assert proxy.data(pidx, QtCore.Qt.BackgroundRole) == model.data(midx, QtCore.Qt.BackgroundRole)

        # Verify proxy.mapFromSource (model -> proxy)
        for row in range(model.rowCount()):
            for col in range(len(app.columns)):
                midx = model.index(row, col)
                pidx = proxy.mapFromSource(midx)
                prow = pidx.row()
                pcol = pidx.column()

                assert pidx.model() == proxy
                assert midx.model() == model

                if model.view[row]['album'] == album:
                    assert model.view[row] == proxy.view[prow]

                    assert proxy.data(pidx, QtCore.Qt.DisplayRole) == model.data(midx, QtCore.Qt.DisplayRole)
                    assert proxy.data(pidx, QtCore.Qt.BackgroundRole) == model.data(midx, QtCore.Qt.BackgroundRole)

                else:
                    assert not pidx.isValid()
                    assert not proxy.pointer(pidx).valid

def test_pyqt_playlist_proxy_mapToSource_invlid(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    pidx = proxy.index(-1, 0)
    midx = proxy.mapToSource(pidx)
    assert not midx.isValid()

def test_pyqt_playlist_proxy_get_mrls(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    rows = range(0,len(view),2)
    mrls = [view[_].mrl for _ in rows]
    indexes = [proxy.index(r,c) for r in rows for c in range(len(app.columns))]
    assert proxy.get_mrls(indexes) == mrls


def test_pyqt_playlist_proxy_play(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    # Filter the proxy for a faster test
    proxy.filter(f"(album={view[0]['album']})")

    # Verify all backgrounds are not set
    for brow in range(len(proxy.view)):
        for bcol in range(len(app.columns)):
            index = proxy.index(brow, bcol)
            background = proxy.data(index, QtCore.Qt.BackgroundRole)

            assert background == QtCore.QVariant()

    prev_playing = -1
    for row in range(len(proxy.view)):
        def check_sig_playing(pointer, length):
            if pointer.record != proxy.view[row]:
                return False
            return True

        def check_sig_data_changed_1(index_top_left, index_bottom_right, roles):
            return (
                QtCore.Qt.BackgroundRole in roles
                and index_top_left.row() == prev_playing
                and index_top_left.column() == 0
                and index_bottom_right.row() == prev_playing
                and index_bottom_right.column() == len(app.columns)-1
            )

        def check_sig_data_changed_2(index_top_left, index_bottom_right, roles):
            return (
                QtCore.Qt.BackgroundRole in roles
                and index_top_left.row() == row
                and index_top_left.column() == 0
                and index_bottom_right.row() == row
                and index_bottom_right.column() == len(app.columns)-1
            )

        with qtbot.waitSignals(
            [
                (proxy.dataChanged, "data_changed_2"),
                (app.signal.playing, "playing"),
            ] if prev_playing < 0 else [
                (proxy.dataChanged, "data_changed_1"),
                (proxy.dataChanged, "data_changed_2"),
                (app.signal.playing, "playing"),
            ],
            check_params_cbs = [
                check_sig_data_changed_2,
                check_sig_playing,
            ] if prev_playing < 0 else [
                check_sig_data_changed_1,
                check_sig_data_changed_2,
                check_sig_playing,
            ],
            order="strict",
        ) as sigs:
            app.signal.playNew.emit(proxy.view.pointer(row))

        assert proxy.is_playing

        for bcol in range(len(app.columns)):
            prev_index = proxy.index(prev_playing, bcol)
            crnt_index = proxy.index(row, bcol)

            prev_background = proxy.data(prev_index, QtCore.Qt.BackgroundRole)
            crnt_background = proxy.data(crnt_index, QtCore.Qt.BackgroundRole)

            assert isinstance(prev_background, QtCore.QVariant)
            assert isinstance(crnt_background, QtGui.QColor)

        prev_playing = row


def test_pyqt_playlist_proxy_persist(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    persist = [QtCore.QPersistentModelIndex(proxy.index(_, 0)) for _ in range(len(view))]

    assert all([_.isValid() for _ in persist])

    retrieved = proxy.persistentIndexList()
    retrieved.sort(key=lambda _:_.row())

    assert len(retrieved) == len(persist)
    assert all(r==p for r,p in zip(retrieved,persist))

    album = view[0]['album']
    proxy.filter(f"(album={album})")

    valid = 0
    for index in persist:
        if index.isValid():
            assert proxy.view[index.row()]['album'] == album
            valid += 1
    assert valid == len(proxy.view)

    retrieved = proxy.persistentIndexList()
    retrieved.sort(key=lambda _:_.row())

    assert len(retrieved) == valid
    assert all(r==p for r,p in zip(retrieved,persist) if p.isValid())

    proxy.sort('title')

    valid = 0
    for index in persist:
        if index.isValid():
            assert proxy.view[index.row()]['album'] == album
            valid += 1
    assert valid == len(proxy.view)

    retrieved = proxy.persistentIndexList()
    retrieved.sort(key=lambda _:_.row())
    persist.sort(key=lambda _:_.row())

    assert len(retrieved) == valid
    assert all(r==p for r,p in zip(retrieved,persist) if p.isValid())

    proxy.filter('')


def test_pyqt_playlist_proxy_select_operations(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    assert not proxy.is_selectable()

    proxy.set_selectable(True)

    assert proxy.is_selectable()

    proxy.setSourceModel(model, selected=QPlaylistFilterSortProxy.SELECT_ALL)
    assert proxy.rowCount() == len(view)
    proxy.setSourceModel(model, selected=[_.mrl for _ in view[:3]])
    assert proxy.rowCount() == 3
    proxy.setSourceModel(model, selected=QPlaylistFilterSortProxy.SELECT_NONE)
    assert proxy.rowCount() == 0

    proxy.select([_.mrl for _ in view[:3]])

    assert list(proxy.view) == view[:3]

    proxy.select_add([view[3]])

    assert list(proxy.view) == view[:4]

    proxy.select_remove([view[0]])

    assert list(proxy.view) == view[1:4]

def test_pyqt_playlist_proxy_mime_drop_init(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    proxy.setSourceModel(model)

    assert not proxy.is_selectable()

    assert not proxy.flags(model.index( 0,0)) & QtCore.Qt.ItemIsDropEnabled
    assert not proxy.flags(model.index(-1,0)) & QtCore.Qt.ItemIsDropEnabled

    assert proxy.supportedDragActions() & QtCore.Qt.CopyAction
    assert not proxy.supportedDragActions() & QtCore.Qt.MoveAction
    assert proxy.supportedDropActions() & QtCore.Qt.CopyAction
    assert not proxy.supportedDropActions() & QtCore.Qt.MoveAction

    proxy.set_selectable(True)

    assert proxy.is_selectable()

    assert proxy.flags(model.index( 0,0)) & QtCore.Qt.ItemIsDropEnabled
    assert proxy.flags(model.index(-1,0)) & QtCore.Qt.ItemIsDropEnabled

    assert proxy.supportedDragActions() & QtCore.Qt.CopyAction
    assert proxy.supportedDragActions() & QtCore.Qt.MoveAction
    assert proxy.supportedDropActions() & QtCore.Qt.CopyAction
    assert proxy.supportedDropActions() & QtCore.Qt.MoveAction

def test_pyqt_playlist_proxy_mime_drop(qtbot, app, backend_mock, library_factory):

    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)
    proxy = QPlaylistFilterSortProxy()
    albums = Counter(_['album'] for _ in view)
    proxy.setSourceModel(model)
    proxy.set_selectable(True)
    proxy.select(proxy.SELECT_NONE)

    assert proxy.rowCount() == 0

    with FsFakePatcher() as patcher:
        # Setup library fake paths
        for filename in (_.filename for _ in view):
            patcher.fs.create_file(str(filename))
            assert pathlib.Path(filename).exists()

        m3u_dir = pathlib.Path('/path/to/music')

        assert m3u_dir.exists()

        # Create M3U File
        m3u_file  = m3u_dir/'test.m3u'

        patcher.fs.create_file(m3u_file, contents='\n'.join(
            [
                '# absolute path',
                str(view[0].filename),
                '# relative path without current directory',
                str(view[1].filename)[15:],
                '# relative path with current directory',
                f'./{str(view[2].filename)[15:]}',
            ]
        ))

        assert m3u_file.exists()

        # Verify M3U File Parsing
        mrls = proxy._parseM3U(m3u_file.as_uri())

        assert len(mrls) == 3
        assert mrls == [_.mrl for _ in view[:3]]

        # Verify error condition (not a file)
        assert proxy._parseM3U('http://foo.com/bar.m3u') == []

        m3u_mime = QtCore.QMimeData()
        m3u_mime.setUrls([QtCore.QUrl(m3u_file.as_uri())])

        assert proxy.rowCount() == 0

        with qtbot.waitSignal(app.signal.playAfter):
            assert proxy.dropMimeData(m3u_mime, QtCore.Qt.CopyAction, -1, 0, QtCore.QModelIndex())

        assert proxy.rowCount() == 3
        assert list(proxy.view) == view[:3]

        mrls_mime = model.mimeData([model.index(_, 0) for _ in range(3,5)])
        assert proxy.dropMimeData(mrls_mime, QtCore.Qt.CopyAction, -1, 0, QtCore.QModelIndex())

        assert proxy.rowCount() == 5
        assert list(proxy.view) == view[:5]

        invalid_mime = QtCore.QMimeData()
        invalid_mime.setText('hello world')

        assert not proxy.dropMimeData(invalid_mime, QtCore.Qt.CopyAction, -1, 0, QtCore.QModelIndex())
