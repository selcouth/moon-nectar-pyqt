import pytest

from PyQt6 import QtCore
from PyQt6 import QtGui

from mnectar.library.view import Sorted
from mnpyqt.core import QPlaylistModel


def test_pyqt_playlist_model_init(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)

    assert model.rowCount() == len(view)
    assert model.columnCount() == len(app.columns)
    assert not model.is_playing
    assert model.playing_index is None

    assert model.mimeTypes() == ['text/uri-list']

    assert model.flags(model.index(0,0)) & QtCore.Qt.ItemIsSelectable
    assert model.flags(model.index(0,0)) & QtCore.Qt.ItemIsEnabled
    assert model.flags(model.index(0,0)) & QtCore.Qt.ItemIsDragEnabled

    assert not model.flags(model.index(len(view),0)) & QtCore.Qt.ItemIsSelectable
    assert not model.flags(model.index(len(view),0)) & QtCore.Qt.ItemIsEnabled
    assert not model.flags(model.index(len(view),0)) & QtCore.Qt.ItemIsDragEnabled

    assert not model.flags(model.index(0,len(app.columns))) & QtCore.Qt.ItemIsSelectable
    assert not model.flags(model.index(0,len(app.columns))) & QtCore.Qt.ItemIsEnabled
    assert not model.flags(model.index(0,len(app.columns))) & QtCore.Qt.ItemIsDragEnabled


def test_pyqt_playlist_model_init_error(app):
    with pytest.raises(ValueError):
        model_error = QPlaylistModel([], app=app)


def test_pyqt_playlist_model_view_changed(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)

    with qtbot.waitSignal(model.modelReset) as sig:
        view.sort("artist")


def test_pyqt_playlist_model_header(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)

    for col in range(len(app.columns)):
        colname = model.headerData(col, QtCore.Qt.Horizontal, QtCore.Qt.DisplayRole)
        assert colname == app.columns[col].description


def test_pyqt_playlist_model_data(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)

    for row in range(len(view)):
        for col in range(len(app.columns)):
            index = model.index(row, col)
            colname = app.columns[col].name

            displayed = model.data(index, QtCore.Qt.DisplayRole, False)
            if colname in view[row] or colname == 'summary':
                expected = app.columns[col].displayFunc(view[row], colname)
                assert displayed == expected
            else:
                assert displayed == ""

            sortstr = model.data(index, QtCore.Qt.DisplayRole, True)
            if colname in view[row] or colname == 'summary':
                expected = app.columns[col].sortFunc(view[row], colname)
                assert sortstr == expected
            else:
                assert sortstr == ""

            tooltip = model.data(index, QtCore.Qt.ToolTipRole)
            assert tooltip == displayed

            assert model.pointer(index).valid
            assert model.pointer(index).view == model.view
            assert model.pointer(index).record == view[row]

    index_invalid = model.index(-1,-1)
    assert not index_invalid.isValid()
    assert not model.pointer(index_invalid).valid


def test_pyqt_playlist_proxy_get_mrls(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)

    rows = range(0,len(view),2)
    mrls = [view[_].mrl for _ in rows]
    indexes = [model.index(r,c) for r in rows for c in range(len(app.columns))]
    assert model.get_mrls(indexes) == mrls


def test_pyqt_playlist_model_play(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)

    # Verify all backgrounds are not set
    for brow in range(len(view)):
        for bcol in range(len(app.columns)):
            index = model.index(brow, bcol)
            background = model.data(index, QtCore.Qt.BackgroundRole)

            assert background == QtCore.QVariant()

    prev_playing = -1
    for row in range(len(view)):
        def check_sig_playing(pointer, length):
            if pointer.record != view[row]:
                return False
            return True

        def check_sig_data_changed_1(index_top_left, index_bottom_right, roles):
            return (
                QtCore.Qt.BackgroundRole in roles
                and index_top_left.row() == prev_playing
                and index_top_left.column() == 0
                and index_bottom_right.row() == prev_playing
                and index_bottom_right.column() == len(app.columns)-1
            )

        def check_sig_data_changed_2(index_top_left, index_bottom_right, roles):
            return (
                QtCore.Qt.BackgroundRole in roles
                and index_top_left.row() == row
                and index_top_left.column() == 0
                and index_bottom_right.row() == row
                and index_bottom_right.column() == len(app.columns)-1
            )

        with qtbot.waitSignals(
            [
                (model.dataChanged, "data_changed_2"),
                (app.signal.playing, "playing"),
            ] if prev_playing < 0 else [
                (model.dataChanged, "data_changed_1"),
                (model.dataChanged, "data_changed_2"),
                (app.signal.playing, "playing"),
            ],
            check_params_cbs = [
                check_sig_data_changed_2,
                check_sig_playing,
            ] if prev_playing < 0 else [
                check_sig_data_changed_1,
                check_sig_data_changed_2,
                check_sig_playing,
            ],
            order="strict",
        ) as sigs:
            app.signal.playNew.emit(model.view.pointer(row))

        assert model.is_playing
        assert model.playing_index == row

        for bcol in range(len(app.columns)):
            prev_index = model.index(prev_playing, bcol)
            crnt_index = model.index(row, bcol)

            prev_background = model.data(prev_index, QtCore.Qt.BackgroundRole)
            crnt_background = model.data(crnt_index, QtCore.Qt.BackgroundRole)

            assert isinstance(prev_background, QtCore.QVariant)
            assert isinstance(crnt_background, QtGui.QColor)

        prev_playing = row

def test_pyqt_playlist_model_mime_drag(qtbot, app, backend_mock, library_factory):
    library = library_factory()
    records = library.records
    view = Sorted(records, app, default="album")
    model = QPlaylistModel(view, app=app)

    sel_rows = list(range(0,len(view),3))
    sel_mrls = [view[_].mrl for _ in sel_rows]
    sel_indexes = [model.index(r,c) for r in sel_rows for c in range(len(app.columns))]

    assert len(sel_indexes) == len(sel_rows) * len(app.columns)

    mime_data = model.mimeData(sel_indexes)

    assert isinstance(mime_data, QtCore.QMimeData)
    assert mime_data.hasUrls()

    urls = mime_data.urls()

    assert len(urls) == len(sel_rows)

    for row in range(len(sel_rows)):
        assert isinstance(urls[row], QtCore.QUrl)
        url = urls[row].url(QtCore.QUrl.FullyEncoded)
        assert url in sel_mrls
        sel_mrls.remove(url)
    assert len(sel_mrls) == 0
