from PyQt6                            import QtCore
from PyQt6                            import QtGui
from PyQt6                            import QtWidgets

class HTMLDelegate(QtWidgets.QStyledItemDelegate):
    def __init__(self, parent=None):
        super().__init__()

    def paint(self, painter, option, index):
        painter.save()

        options = QtWidgets.QStyleOptionViewItem(option)

        self.initStyleOption(options, index)

        doc = QtGui.QTextDocument(self)
        doc.setHtml(options.text)
        doc.setTextWidth(options.rect.width())
        options.text = ""

        style = QtCore.QApplication.style() if options.widget is None else options.widget.style()
        style.drawControl(QtWidgets.QStyle.ControlElement.CE_ItemViewItem, options, painter)

        ctx = QtGui.QAbstractTextDocumentLayout.PaintContext()

        if option.state & QtWidgets.QStyle.StateFlag.State_Selected:
            ctx.palette.setColor(QtGui.QPalette.Text, option.palette.color(
                QtGui.QPalette.ColorGroup.Active, QtGui.QPalette.ColorRole.HighlightedText))
        else:
            ctx.palette.setColor(QtGui.QPalette.ColorRole.Text, option.palette.color(
                QtGui.QPalette.ColorGroup.Active, QtGui.QPalette.ColorRole.Text))

        textRect = style.subElementRect(
            QtWidgets.QStyle.SubElement.SE_ItemViewItemText, options)

        if index.column() != 0:
            textRect.adjust(5, 0, 0, 0)

        painter.translate(textRect.topLeft())
        painter.setClipRect(textRect.translated(-textRect.topLeft()))
        doc.documentLayout().draw(painter, ctx)

        painter.restore()

    def sizeHint(self, option, index):
        options = QtWidgets.QStyleOptionViewItem(option)
        self.initStyleOption(options,index)

        doc = QtGui.QTextDocument()
        doc.setHtml(options.text)
        doc.setTextWidth(options.rect.width())
        return QtCore.QSize(int(doc.idealWidth()), int(doc.size().height()))


