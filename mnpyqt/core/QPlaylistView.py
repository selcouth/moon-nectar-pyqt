import logging

from enum         import Enum, unique
from weakref      import finalize
from PyQt6        import QtCore
from PyQt6        import QtGui
from PyQt6        import QtWidgets
from PyQt6.QtCore import Qt
from PyQt6.QtCore import pyqtProperty

from mnectar.action import Action
from mnectar.config import Setting

from .action import QActionable

_logger = logging.getLogger("mnectar."+__name__)

class QPlaylistView(QtWidgets.QTableView, QActionable):
    """A PyQt view customized for display of a music playlist"""

    CAN_DRAG   = True
    CAN_DROP   = False
    CAN_DELETE = False

    savedState = Setting(default=None)
    _name      = 'Unknown'

    @pyqtProperty(bool)
    def playing(self):
        return self.model() is not None and self.app.state.playing.view == self.model().view

    def __init_subclass__(cls, can_drag = None, can_drop = None, can_delete = None, **kw):
        super().__init_subclass__(**kw)

        if can_drag   is not None: cls.CAN_DRAG   = can_drag
        if can_drop   is not None: cls.CAN_DROP   = can_drop
        if can_delete is not None: cls.CAN_DELETE = can_delete

    def __init__(self, *arg, can_drag = None, can_drop = None, can_delete = None, **kw):
        super().__init__(*arg, **kw)

        if can_drag   is not None: self.CAN_DRAG   = can_drag
        if can_drop   is not None: self.CAN_DROP   = can_drop
        if can_delete is not None: self.CAN_DELETE = can_delete

        self.config_ui()
        self.config_signal()

        QtCore.QCoreApplication.instance().aboutToQuit.connect(self.on_close)

    def name(self):
        return self._name

    def setName(self, name):
        self._name = name

        self.setObjectName(name)

        self.setStyleSheet(f"""
            QPlaylistView#{self.name()}[playing=true] {{
                border: 2px solid green;
            }}
            QPlaylistView#{self.name()}[playing=false] {{
                border: 2px solid gray;
            }}
        """)

    def sizeHintForColumn(self, column):
        return self.window().app.columns[column].sizeHint

    def on_close(self):
        _logger.debug(f"Close Event ... saving playlist view: {self.name()}")
        self.savedState = self.saveState()

    def saveState(self):
        header = self.horizontalHeader()
        return {
            'columns': [
                {
                    'visualIndex':  col,
                    'logicalIndex': header.logicalIndex(col),
                    'hidden':       header.isSectionHidden(header.logicalIndex(col)),
                    'size':         header.sectionSize(header.logicalIndex(col)),
                    'name':         header.model().headerData(col, Qt.Orientation.Horizontal, Qt.ItemDataRole.DisplayRole),
                }

                for col in range(header.count())
            ],

            'sortIndicatorShown':   header.isSortIndicatorShown(),
            'sortIndicatorSection': header.sortIndicatorSection(),
            'sortIndicatorOrder':   header.sortIndicatorOrder().name,
        }

    def restoreState(self, state):
        if state is not None:
            header = self.horizontalHeader()
            headers = {
                header.model().headerData(_, Qt.Orientation.Horizontal, Qt.ItemDataRole.DisplayRole): _
                for _ in range(self.model().columnCount())
            }

            header.setSortIndicatorShown(state['sortIndicatorShown'])

            if state['sortIndicatorShown']:
                header.setSortIndicator(state['sortIndicatorSection'], QtCore.Qt.SortOrder[state['sortIndicatorOrder']])

            for column in state['columns']:
                vi = column['visualIndex']
                li = column['logicalIndex']

                if vi != li:
                    _logger.debug("Column Reorder Not Supported")

                if column['name'] in headers:
                    header_index = headers[column['name']]
                    header.setSectionHidden(header_index, column['hidden'])
                    header.resizeSection(header_index, column['size'])

                    if header_index != li:
                        _logger.debug(f"Column internal reorder detected for '{column['name']}': {li} -> {header_index}")
                else:
                    _logger.debug(f"Column no no longer exists: {column['name']}")

    def config_ui(self):
        """Configure the widget"""

        if self.CAN_DRAG and self.CAN_DROP:
            dragDropMode = self.DragDropMode.DragDrop
            dropAction   = Qt.DropAction.MoveAction
        elif self.CAN_DRAG:
            dragDropMode = self.DragDropMode.DragOnly
            dropAction   = Qt.DropAction.IgnoreAction
        elif self.CAN_DROP:
            dragDropMode = self.DragDropMode.DropOnly
            dropAction   = Qt.DropAction.CopyAction
        else:
            dragDropMode = self.DragDropMode.NoDragDrop
            dropAction   = Qt.DropAction.IgnoreAction

        self.verticalHeader().setVisible(False)                           # No left-side header
        self.setSortingEnabled(True)                                      # Sorting is enabled
        self.setSelectionBehavior(self.SelectionBehavior.SelectRows)      # Select entire rows at a time (not cells)
        self.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu) # Enable right-click context menu
        self.setAlternatingRowColors(True)                                # Alternating row colors
        self.setWordWrap(False)                                           # Do not wrap words (tooltips are used instead)
        self.setDragDropOverwriteMode(False)                              # Drop will always insert, never overwrite!
        self.setDragEnabled(self.CAN_DRAG)                                # drag source?
        self.setAcceptDrops(self.CAN_DROP)                                # Drop destination?
        self.viewport().setAcceptDrops(self.CAN_DROP)                     # Drop destination?
        self.setDropIndicatorShown(self.CAN_DROP)                         # Show the drop indicator?
        self.setDragDropMode(dragDropMode)                                # Set the drag/drop mode
        self.setDefaultDropAction(dropAction)                             # Action on drop
        self.setStyle(_DropRowStyle())                                    # Style Override: Entire Row drop indicator

        self.horizontalHeader().show()                                                       # Show the horizontal (top) header
        self.horizontalHeader().setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu) # Enable context menu on the header

    def config_signal(self):
        """Configure signals for the widget"""
        self.activated.connect(self.on_activated)
        self.customContextMenuRequested.connect(self.on_context_menu)
        self.horizontalHeader().customContextMenuRequested.connect(self.on_header_context_menu)
        self.window().app.signal.playing.connect(self.on_playing)

    def setModel(self, model):
        super().setModel(model)
        # Resize the columns to contents (or default sizes)
        # ... These are default widths
        # ... Set here because column names and widths come from the model
        self.resizeColumnsToContents()

        # Set default hidden columns
        # ... this happens before restoring any saved state
        # ... as saved states take precedence!
        for column in self.window().app.columns.index(hidden=True):
            self.setColumnHidden(column, True)

        # Some header configuration must happen after the model has been set
        self.horizontalHeader().setSectionsMovable(True)      # Columns can be moved via drag/drop
        self.restoreState(self.savedState) # Restore the table layout

    def on_context_menu(self, pos):
        menu = QtWidgets.QMenu()
        for action in self.actions():
            menu.addAction(action)
        self.get_context_menu_actions(menu)
        action = menu.exec(self.mapToGlobal(pos))

    def on_header_context_menu(self, pos):
        if not hasattr(self, '_header_context_menu'):
            def getcb(col):
                def func(show):
                    self.setColumnHidden(col, not show)
                    if show:
                        self.resizeColumnToContents(col)
                return func

            menu = self._header_context_menu = QtWidgets.QMenu()

            for idx,column in enumerate(self.window().app.columns.values()):
                qaction = QtGui.QAction(column.description, self)
                qaction.setCheckable(True)
                qaction.setChecked(not self.isColumnHidden(idx))
                qaction.triggered.connect(getcb(idx))

                menu.addAction(qaction)

        action = self._header_context_menu.exec(self.mapToGlobal(pos))

    def get_context_menu_actions(self, menu):
        pass

    def selectedRows(self):
        return set(_.row() for _ in self.selectedIndexes())

    def selectionChanged(self, selected, deselected):
        super().selectionChanged(selected, deselected)
        mrls = self.model().get_mrls(self.selectedIndexes())
        self.window().app.ui.selected.select(mrls)

    def get_vindex_from_mrl(self, mrl):
        # FIXME: This will fail with duplicate playlist entries!

        playlist_index = self.model().index_of_mrl(mrl)
        if playlist_index is None:
            return QtCore.QModelIndex()
        else:
            mindex = self.model().index(playlist_index, 0)
            return mindex

    def on_activated(self, vindex):
        if vindex.isValid():
            self.window().app.signal.playNew.emit(self.model().pointer(vindex))

    def on_playing(self, pointer, length):
        if pointer.view == self.model().view:
            index = pointer.view_index
            if index is not None:
                # NOTE: Qt has strange behavior where scrollTo fails to work unless the
                # first column is visible.  So if that column is visible, it must be
                # unhidden before scrolling.  Simply selecting a different unhidden
                # column is NOT sufficient!

                header = self.horizontalHeader()
                hidden = header.isSectionHidden(header.logicalIndex(0))
                qidx = self.model().index(index, 0)

                if hidden:
                    self.setColumnHidden(0, False)

                self.scrollTo(self.model().index(index, 0), self.ScrollHint.EnsureVisible)

                if hidden:
                    self.setColumnHidden(0, True)

            self.style().unpolish(self)
            self.style().polish(self)
            self.update()
        else:
            self.style().unpolish(self)
            self.style().polish(self)
            self.update()

class _DropRowStyle(QtWidgets.QProxyStyle):
    def drawPrimitive(self, element, option, painter, widget=None):
        """
        Draw a line across the entire row rather than just the column we're hovering over.
        NOTE: This may not always work depending on global style or operating system
        """
        if element == self.PrimitiveElement.PE_IndicatorItemViewItemDrop and not option.rect.isNull():
            option_new = QtWidgets.QStyleOption(option)
            option_new.rect.setLeft(0)
            if widget:
                option_new.rect.setRight(widget.width())
            option = option_new
        super().drawPrimitive(element, option, painter, widget)

