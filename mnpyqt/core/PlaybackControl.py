import logging
import os

from PyQt6               import QtWidgets
from PyQt6               import QtCore
from PyQt6               import QtGui
from PyQt6.QtCore        import Qt

from .PlaybackControl_UI import Ui_PlaybackControl
from .Dockable           import Dockable

_logger = logging.getLogger("mnectar."+__name__)

class PlaybackControl(QtWidgets.QWidget, Dockable,
        location=Qt.DockWidgetArea.TopDockWidgetArea):

    DOWN_DELAY = 100

    play_pause_block_next = False

    def __init__(self, *arg, **kw):
        super().__init__(*arg, **kw)

        self.ui = Ui_PlaybackControl()
        self.ui.setupUi(self)
        self.config_ui()

    def config_ui(self):
        self.ui.Volume.setValue(self.window().app.state.backend.volume)

        self.ui.PlayPause .toggled.connect(self.on_play_pause)
        self.ui.Stop      .clicked.connect(self.on_stop)
        self.ui.Next      .clicked.connect(self.on_next)
        self.ui.Prev      .clicked.connect(self.on_previous)
        self.ui.Volume    .valueChanged.connect(self.on_volume_changed)

        self.window().app.signal.stopped          .connect(self.on_stopped_ext)
        self.window().app.signal.playing          .connect(self.on_playing_ext)
        self.window().app.signal.paused           .connect(self.on_paused_ext)
        self.window().app.signal.mmkey_playNext   .connect(self.on_next_external)
        self.window().app.signal.mmkey_playPrev   .connect(self.on_prev_external)
        self.window().app.signal.mmkey_togglePause.connect(self.on_togglePause_external)

        # FIXME: 2021/01/08: PyQt6 removes Qt resource files so icons must be loaded manually
        # FIXME:             Find a way to NOT hard code icon filenames into the code
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(os.path.join(os.path.dirname(__file__),"icons/Media-skip-backward.svg")), QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.ui.Prev.setIcon(icon)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(os.path.join(os.path.dirname(__file__),"icons/Media-playback-start.svg")), QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        icon1.addPixmap(QtGui.QPixmap(os.path.join(os.path.dirname(__file__),"icons/Media-playback-pause.svg")), QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.On)
        self.ui.PlayPause.setIcon(icon1)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(os.path.join(os.path.dirname(__file__),"icons/Media-playback-stop.svg")), QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.ui.Stop.setIcon(icon2)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(os.path.join(os.path.dirname(__file__),"icons/Media-skip-forward.svg")), QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.ui.Next.setIcon(icon3)

    def on_volume_changed(self, volume):
        self.window().app.signal.setVolume.emit(volume)

    def on_playing_ext(self, mrl, length):
        if not self.ui.PlayPause.isChecked() and not self.ui.PlayPause.isDown():
            self.ui.PlayPause.setDown(True)
            QtCore.QTimer.singleShot(self.DOWN_DELAY, self.on_playing_delay)

    def on_paused_ext(self):
        if self.ui.PlayPause.isChecked() and not self.ui.PlayPause.isDown():
            self.ui.PlayPause.setDown(True)
            QtCore.QTimer.singleShot(self.DOWN_DELAY, self.on_pause_delay)

    def on_next_external(self):
        self.ui.Next.setDown(True)
        QtCore.QTimer.singleShot(self.DOWN_DELAY, self.on_next_delay)

    def on_prev_external(self):
        self.ui.Prev.setDown(True)
        QtCore.QTimer.singleShot(self.DOWN_DELAY, self.on_prev_delay)

    def on_togglePause_external(self):
        self.ui.PlayPause.setDown(True)
        QtCore.QTimer.singleShot(self.DOWN_DELAY, self.on_togglePause_delay)

    def on_togglePause_delay(self):
        self.ui.PlayPause.setDown(False)
        if self.ui.PlayPause.isChecked():
            self.ui.PlayPause.blockSignals(True)
            self.ui.PlayPause.setChecked(False)
            self.ui.PlayPause.blockSignals(False)
        else:
            self.ui.PlayPause.blockSignals(True)
            self.ui.PlayPause.setChecked(True)
            self.ui.PlayPause.blockSignals(False)

    def on_next_delay(self):
        self.ui.Next.setDown(False)

    def on_prev_delay(self):
        self.ui.Prev.setDown(False)

    def on_pause_delay(self):
        if self.ui.PlayPause.isDown():
            self.ui.PlayPause.setDown(False)
            # This method is for synchonization with the rest of the app
            # ... so block signals when changing the checked state
            # ... or any behavior around the button might trigger!
            self.ui.PlayPause.blockSignals(True)
            self.ui.PlayPause.setChecked(False)
            self.ui.PlayPause.blockSignals(False)

    def on_playing_delay(self):
        if self.ui.PlayPause.isDown():
            self.ui.PlayPause.setDown(False)
            # This method is for synchonization with the rest of the app
            # ... so block signals when changing the checked state
            # ... or any behavior around the button might trigger!
            self.ui.PlayPause.blockSignals(True)
            self.ui.PlayPause.setChecked(True)
            self.ui.PlayPause.blockSignals(False)

    def on_stopped_ext(self):
        # This method is for synchonization with the rest of the app
        # ... so block signals when changing the checked state
        # ... or any behavior around the button might trigger!
        self.ui.PlayPause.blockSignals(True)
        self.ui.PlayPause.setChecked(False)
        self.ui.PlayPause.blockSignals(False)

    def on_play_pause(self, state):
        self.play_pause_block_next = True
        if state:
            self.window().app.signal.play.emit()
        else:
            self.window().app.signal.pause.emit()

    def on_next(self):
        self.play_pause_block_next = True
        self.window().app.signal.playNext.emit()

    def on_previous(self):
        self.play_pause_block_next = True
        self.window().app.signal.playPrev.emit()

    def on_stop(self):
        self.window().app.signal.stop.emit()
