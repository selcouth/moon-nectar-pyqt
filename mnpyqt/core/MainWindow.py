import logging

from PyQt6               import QtWidgets
from PyQt6               import QtGui
from PyQt6               import QtCore
from .MainWindow_UI      import Ui_MainWindow
from .PlaybackControl    import PlaybackControl
from .PlaybackStatusText import PlaybackStatusCoverText
from .PlaybackStatusText import PlaybackStatusText
from .browser            import BrowserContainer
from .settings           import SettingsDialog
from .action             import QActionable
from .Dockable           import Dockable
from mnectar.registry    import Registry
from mnectar.config      import Setting
from mnectar.action      import Action


_logger = logging.getLogger("mnectar."+__name__)

class MainWindow(QtWidgets.QMainWindow, QActionable):
    savedState    = Setting(default = False)
    savedGeometry = Setting(default = False)

    dock_title_visible = Setting("ui.dock.title.visible", default = True)

    toggle_dock_title = Action("View", "Layout", "Show Dock Titles",
                                   checkable=True,
                                   setting=dock_title_visible)

    def __init__(self, app=None, *arg, **kw):
        self.app = app
        super().__init__(*arg, **kw)

        self.ui  = Ui_MainWindow()
        self.ui.setupUi(self)

        self.config_ui()

        self.show()

        self.app.signal.initComplete.connect(self.onInitComplete)

    def onInitComplete(self):
        if self.savedGeometry:
            self.restoreGeometry(self.savedGeometry)
        if self.savedState:
            self.restoreState(self.savedState)

    def closeEvent(self, event):
        _logger.debug("Close Event ... saving window state")
        self.savedGeometry = self.saveGeometry().data()
        self.savedState    = self.saveState().data()

    def config_ui(self):
        self.setWindowTitle("Moon Nectar Media Player")
        self.ui.playbackcontrol = PlaybackControl(self, app=self.app)
        self.ui.playbackcontroldock = self.ui.playbackcontrol.make_dock_widget(self)

        self.ui.playbackstatus = PlaybackStatusCoverText(self, app=self.app)
        self.ui.playbackstatusdock = self.ui.playbackstatus.make_dock_widget(self)

        self.ui.browser = self.app.ui.browser = BrowserContainer(parent=self)
        self.setCentralWidget(self.app.ui.browser)

        self.menus = {}

        self.settings_action = QtGui.QAction("settings", self)
        self.settings_action.triggered.connect(self.show_settings)
        self.settings_menu = self.ui.menubar.addMenu("settings")
        self.settings_menu.addAction(self.settings_action)

        self.setStyleSheet("""
            #QDockWidgetTitle {
                border-bottom: 1px solid #999;
                background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #EEE, stop: 0.3 #AAA, stop: 0.6 #999099, stop: 1 #998899);
            }
        """)

        self.on_toggle_dock_title(self.dock_title_visible)

    @toggle_dock_title.triggered
    def on_toggle_dock_title(self, state):
        dock_titles = self.findChildren(QtWidgets.QWidget, "QDockWidgetTitle")
        if state:
            for widget in dock_titles:
                widget.layout().setContentsMargins(0,10,0,0)
        else:
            for widget in dock_titles:
                widget.layout().setContentsMargins(0,0,0,0)

    def show_settings(self):
        obj = SettingsDialog(self.app, self)
        obj.exec()

    def addMenuItem(self, menu_name, name, action, shortcut=None):
        menu = self.getMenu(menu_name)

        menu.addAction(name, action, shortcut)

    def getMenu(self, menu_name):
        menu = self.ui.menubar.findChild(QtWidgets.QMenu, menu_name, QtCore.Qt.FindChildOption.FindDirectChildrenOnly)
        if not menu:
            menu = self.ui.menubar.addMenu(menu_name)
            menu.setObjectName(menu_name)
        return menu
