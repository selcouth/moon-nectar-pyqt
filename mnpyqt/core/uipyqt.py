import functools
import sys
import logging

from PyQt6 import QtWidgets

from .MainWindow import MainWindow
from .pyqtsignal import QSignalControl
from .QPlaylistModel import QPlaylistModel
from .ActionCreator import ActionCreator
from mnectar.util import oscheck
from mnectar.config import Setting
from mnectar.library.view import Selected

import Registry.UI

_logger = logging.getLogger("mnectar."+__name__)


class GuiPyQt(Registry.UI.UiPlugin, registry=Registry.UI, optname="pyqt"):
    def __init__(self, app=None, *arg, **kw):
        super().__init__(app=app, *arg, **kw)

        # Flag indicating the PyQt GUI is in use
        # ... useful for plugins to determine if they should load
        self.pyqt = True

        self.app.signal.pyqt = QSignalControl(self.app)

        # Create the application
        self.uiapp = QtWidgets.QApplication([sys.argv])

    @oscheck(target="Darwin")
    def macos_disable_tab_bar(self):
        """
        Disable MacOS automatic tab bar functionality, which can only occur via the
        native interface.
        """
        try:
            import AppKit

            AppKit.NSWindow.setAllowsAutomaticWindowTabbing_(False)
        except ImportError:
            pass

    def patch_setting(self):
        """
        patch the Setting class to work with PyQt objects so that the app instance can
        be stored in the main window object without necessarily needing to store it in
        each instance.
        """

        def pyqt_getSettingManager(function):
            @functools.wraps(function)
            def wrapper(self, instance, app=None):
                if app is None and hasattr(instance, 'app'):
                    app = instance.app
                elif app is None and isinstance(instance, QtWidgets.QWidget):
                    app = instance.window().app
                return function(self, instance, app=app)

            return wrapper

        Setting._getSettingManager = pyqt_getSettingManager(Setting._getSettingManager)

    def init(self):
        # Cleanup macos behavior
        self.macos_disable_tab_bar()

        # Patch the Setting class to work better with PyQt
        self.patch_setting()

        # Get the library content
        self.library_view = self.app.library.content.records

        # Setup the selected records view
        self.selected = Selected(self.library_view)

        # Create the global library browser model
        self.browser_model = QPlaylistModel(self.library_view, app=self.app)

        # Create the global selected record model
        self.selected_model = QPlaylistModel(self.selected, app=self.app)

        # Create the main window
        self.main = MainWindow(self.app)

        # Set the main window as active in the application
        self.uiapp.setActiveWindow(self.main)

        # Create all browser widgets
        Registry.UI.PyQt.Browsers.create_all(self.app, self.main)

        # Create all docked plugins
        Registry.UI.PyQt.Docked.create_all(self.app, self.main)

        # Create all menu plugins
        Registry.UI.PyQt.Menu.create_all(self.app, self.main)

        # Create all auto actions
        self.action_manager = ActionCreator(self.main, app=self.app)
        self.action_manager.create_menu_actions(self.main.ui.menubar)

    def run(self):
        # Run the application event loop
        exit_code = self.uiapp.exec()

        # Destroy the backend!
        return exit_code
