import logging
import pathlib
import urllib

from contextlib   import contextmanager
from PyQt6        import QtCore
from PyQt6        import QtGui
from PyQt6.QtCore import Qt

from mnectar.library.view import Changed, View, ViewPointer

from .roles import UserRoles

_logger = logging.getLogger("mnectar."+__name__)

class QPlaylistModel(QtCore.QAbstractTableModel):
    _playing = ViewPointer()

    @property
    def is_playing(self):
        return self.app.state.playing.pointer.valid and self.view in self.app.state.playing.view.chain

    @property
    def playing_index(self):
        return self._view_playing_index(self.view)

    def _view_playing_index(self, view):
        """
        This method permits reuse of the 'playing_index' functionality by proxy classes.
        """

        if self._playing.valid and self._playing.record in view:
            return view.index(self._playing.record)
        else:
            return None

    def __init__(self, view, *arg, app=None, **kw):
        super().__init__(*arg, **kw)
        self.app = app

        if not isinstance(view, View):
            raise ValueError(f"Provided model view is not a valid library View object!")

        self.view = Changed(view)
        self.app.signal.playing.connect(self.on_playing)
        self.view.changed.connect(self.on_view_changed)

    def on_view_changed(self, *arg):
        self.modelReset.emit()

    def on_playing(self, pointer, length):
        if self._playing.valid:
            # Remove the old background color row indicator
            row = self.playing_index
            self._playing = ViewPointer()
            # ... and notify Qt of the change
            self.notify_background_changed(row)

        if self.view in pointer.view.chain:
            # Save the new background color row indicator
            self._playing = pointer
            row = self.playing_index
            # ... and notify Qt of the change
            self.notify_background_changed(row)

    def notify_background_changed(self, row):
        """
        Notify Qt that the background has changed for a single row of data.
        """
        change_from_index = [
            self.createIndex(row, 0),
            self.createIndex(row, len(self.app.columns)-1),
            [QtCore.Qt.ItemDataRole.BackgroundRole]]

        self.dataChanged.emit(*change_from_index)

    def pointer(self, index):
        if index.isValid():
            return self.view.pointer(index.row())
        else:
            return ViewPointer(app=self.app, view=self.view)

    def get_mrls(self, indexes):
        # Use dict (always ordered) as an ordered set
        rows = {_.row(): None for _ in indexes}
        return [self.view[_].mrl for _ in rows]

    def rowCount(self, parent=None):
        return len(self.view)

    def columnCount(self, parent=None):
        return len(self.app.columns)

    def flags(self, index):
        if index.isValid():
            return (
                super().flags(index)
                | QtCore.Qt.ItemFlag.ItemIsSelectable
                | QtCore.Qt.ItemFlag.ItemIsEnabled
                | QtCore.Qt.ItemFlag.ItemIsDragEnabled
            )
        else:
            return super().flags(index)

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Orientation.Horizontal:
            if role == QtCore.Qt.ItemDataRole.DisplayRole:
                return self.app.columns[section].description

    def data(self, index, role, trackStrSort=False, view=None):
        if view is None:
            view = self.view

        if role == QtCore.Qt.ItemDataRole.DisplayRole:
            try:
                record = view[index.row()]
                column = self.app.columns[index.column()].name
                if trackStrSort:
                    value = self.app.columns[index.column()].sortFunc(record, column)
                else:
                    value = self.app.columns[index.column()].displayFunc(record, column)
                return value
            except KeyError as ex:
                return ""

        elif role == QtCore.Qt.ItemDataRole.BackgroundRole:
            if index.row() == self._view_playing_index(view):
                return QtGui.QColor(QtGui.QColor(150,150,200))
            else:
                return QtCore.QVariant()

        elif role == QtCore.Qt.ItemDataRole.ToolTipRole:
            return self.data(index, QtCore.Qt.ItemDataRole.DisplayRole, view=view)

    def mimeTypes(self):
        return ['text/uri-list']

    def mimeData(self, indexes):
        rows = list({_.row():0 for _ in indexes})
        mrls = [QtCore.QUrl(self.view[_].mrl) for _ in rows]
        data = QtCore.QMimeData()
        data.setUrls(mrls)
        return data
