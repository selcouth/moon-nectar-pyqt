import logging

_logger = logging.getLogger("mnectar."+__name__)

from PyQt6 import QtWidgets
from PyQt6 import QtGui
from PyQt6 import QtCore

from mnectar.action import Action, Actionable
from .action import QActionable

class ActionCreator(QtCore.QObject, QActionable):
    def __init__(self, *arg, app=None, **kw):
        self.app = self.app or app

        super().__init__(*arg, app=app, **kw)

        if self.app is not None and not hasattr(self.app, "_actionable_instances"):
            self.app._actionable_instances = []

    def create_menu_actions(self, target_menu):
        parent = self
        actions = []

        for obj in self.app._actionable_instances:
            actions.extend(obj.actionables.values())

        actions.sort(
            key=lambda _: (
                *[
                    (menu, "") if not _.menu.endswith(menu) else (menu, _.group)
                    for menu in _.menu.split("|")
                ],
                (_.name, ""),
            )
        )

        self._qgroups = qgroups = {}

        prev = None

        for action in actions:
            if action.name:
                if not hasattr(action, 'qaction'):
                    self._create_qaction(action)

                parent_menu = target_menu
                for submenu in action.menu.split("|"):
                    submenu_obj = parent_menu.findChild(
                        QtWidgets.QMenu, submenu, QtCore.Qt.FindChildOption.FindDirectChildrenOnly
                    )
                    if not submenu_obj:
                        submenu_obj = QtWidgets.QMenu(submenu, parent=parent_menu)
                        submenu_obj.setObjectName(submenu)
                        parent_menu.addMenu(submenu_obj)
                    parent_menu = submenu_obj

                if (
                    prev
                    and (
                        action.menu.startswith(prev.menu)
                        or prev.menu.startswith(action.menu)
                    )
                    and prev.group != action.group
                ):
                    parent_menu.addSeparator()

                if action.group:
                    if not action.group in qgroups:
                        group = QtGui.QActionGroup(self)
                        qgroups[action.group] = group
                    else:
                        group = qgroups[action.group]
                    group.setExclusive(action.exclusive)
                    group.addAction(action.qaction)

                if action.menu:
                    parent_menu.addAction(action.qaction)

                prev = action

