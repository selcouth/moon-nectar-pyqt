import functools

from PyQt6 import QtGui
from PyQt6 import QtWidgets

from mnectar.action import Action, Actionable

class QActionable(Actionable):
    def __init__(self, *arg, app=None, **kw):
        self.app = self.app or app

        if self.app is None and hasattr(self.window(), 'app'):
            self.app = self.window().app

        super().__init__(*arg, **kw)

        for action in self.actionables.values():
            self._create_qaction(action)

    def _create_qaction(self, action: Action):
        """
        Update a specified action object with a PyQt QAction.

        The new object will be accessible as `action.qaction`

        :param action: The mnectar action to update
        """
        qaction = action.qaction = QtGui.QAction(action.name, self)
        qaction.setCheckable(action.checkable)

        if action.shortcut:
            qaction.setShortcut(action.shortcut)

        if action.checkable:
            qaction.setChecked(action.is_checked())

        qaction.triggered.connect(action.on_triggered)

        action.set_shortcut_change_callback(
            functools.partial(self._update_sequence, action, qaction)
        )

    def _update_sequence(self, action, qaction, sequence):
        qaction.setShortcut(QtGui.QKeySequence.fromString(sequence))


