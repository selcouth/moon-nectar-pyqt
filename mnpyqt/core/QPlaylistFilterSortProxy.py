import logging

_logger = logging.getLogger("mnectar."+__name__)

import pathlib
import urllib

from contextlib import contextmanager
from typing import List

from PyQt6 import QtCore
from PyQt6 import QtGui
from PyQt6 import QtWidgets
from PyQt6.QtCore import Qt

from mnectar.library.view import Selected, Sorted, Filtered, ViewPointer

from .QPlaylistModel import QPlaylistModel


#class QPlaylistFilterSortProxy(QtCore.QAbstractProxyModel):
class QPlaylistFilterSortProxy(QtCore.QSortFilterProxyModel):
    SELECT_ALL = '*'
    SELECT_NONE = []

    _selectable = False

    def setSourceModel(self, sourceModel, selected=SELECT_ALL):
        if not isinstance(sourceModel, QPlaylistModel):
            raise ValueError("This proxy is only valid for QPlaylistModel instances!")
        else:
            self.app = sourceModel.app
            super().setSourceModel(sourceModel)
            self.view = Sorted(Filtered(Selected(self.source_view, default=selected)), default=Sorted.DEFAULT_UNSORTED)

            # Connect the dataChanged signal
            # ... this does not happen automatically and is necessary for proper updates!
            sourceModel.dataChanged.connect(self.on_source_dataChanged)
            super().setSourceModel(sourceModel)

    @property
    def is_playing(self):
        return self.app.state.playing.pointer.valid and self.view in self.app.state.playing.view.chain

    @property
    def source_view(self):
        return self.sourceModel().view

    def pointer(self, index):
        if index.isValid():
            return self.view.pointer(index.row())
        else:
            return ViewPointer(app=self.app, view=self.view)

    def on_source_dataChanged(self, top_left, bottom_right, roles):
        self.dataChanged.emit(
            self.mapFromSource(top_left),
            self.mapFromSource(bottom_right),
            roles,
        )

    def mapFromSource(self, sourceIndex):
        row = sourceIndex.row()
        rec = self.source_view[row]
        if rec in self.view:
            return self.createIndex(self.view.index(rec), sourceIndex.column())
        else:
            return self.createIndex(-1, sourceIndex.column())

    def mapToSource(self, proxyIndex):
        row = proxyIndex.row()
        if 0 <= row < len(self.view):
            rec = self.view[row]
            return self.sourceModel().createIndex(self.source_view.index(rec), proxyIndex.column())
        else:
            return self.sourceModel().createIndex(-1, proxyIndex.column())

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.view)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return self.sourceModel().columnCount()

    def get_mrls(self, indexes):
        # Use dict (always ordered) as an ordered set
        rows = {_.row(): None for _ in indexes}
        return [self.view[_].mrl for _ in rows]

    def parent(self, index):
        return QtCore.QModelIndex()

    @contextmanager
    def _layoutChangeManager(self):
        try:
            # Send a signal that the layout will be changing
            self.layoutAboutToBeChanged.emit([], self.LayoutChangeHint.VerticalSortHint)

            # Save the old persistent index list
            # ... so they can be updated later
            old_persist_list = self.persistentIndexList()
            old_index_rows = [x.row() for x in old_persist_list]
            old_index_records = [self.view[x] for x in old_index_rows]

            yield
        finally:
            # Create the new persistent index objects

            if len(old_persist_list) > 0:
                new_index_rows = [
                    self.view.index(rec) if rec in self.view else None
                    for rec in old_index_records
                ]
                new_persist_list = [
                    QtCore.QModelIndex()
                    if row is None
                    else self.index(row, old_index.column(), old_index.parent())
                    for row, old_index in zip(new_index_rows, old_persist_list)
                ]
                self.changePersistentIndexList(old_persist_list, new_persist_list)

            self.layoutChanged.emit([], self.LayoutChangeHint.VerticalSortHint)

    def sort(self, column=None, order=Qt.SortOrder.AscendingOrder):
        with self._layoutChangeManager():
            if column is None:
                self.view.sort(None, order != Qt.SortOrder.AscendingOrder)
            else:
                self.view.sort(
                    self.app.columns[column].name, order != Qt.SortOrder.AscendingOrder
                )

    def filter(self, filterstr=None):
        with self._layoutChangeManager():
            self.view.filter(filterstr)

    def data(self, index, role, trackStrSort=False):
        # Qt proxies are very slow looking up data via mapToSource/mapFromSource
        # ... to fix this, access the proxy view directly
        # ... but to cut down on code duplication, call QModelView.data(...)
        # ... passing in our view as the data source so all indexing is correct
        return self.sourceModel().data(index, role, trackStrSort, self.view)

    def select(self, mrls=SELECT_ALL):
        with self._layoutChangeManager():
            self.view.select(mrls)

    def select_add(self, mrls):
        with self._layoutChangeManager():
            self.view.select_add(mrls)

    def select_remove(self, mrls):
        with self._layoutChangeManager():
            self.view.select_remove(mrls)

    def set_selectable(self, state: bool):
        self._selectable = state

    def is_selectable(self):
        return self._selectable

    def supportedDropActions(self):
        if self.is_selectable():
            return Qt.DropAction.CopyAction | Qt.DropAction.MoveAction
        else:
            return super().supportedDropActions()

    def supportedDragActions(self):
        if self.is_selectable():
            return Qt.DropAction.CopyAction | Qt.DropAction.MoveAction
        else:
            return super().supportedDragActions()

    def flags(self, index):
        if self.is_selectable():
            if index.isValid():
                return super().flags(index) | Qt.ItemFlag.ItemIsDropEnabled
            else:
                return super().flags(index) | Qt.ItemFlag.ItemIsDropEnabled
        else:
            return super().flags(index)

    def _parseM3U(self, mrl: str) -> List[str]:
        """
        Parse an M3U file.

        :param mrl: The MRL of the M3U file.
        :returns: A list of files contained in the M3U file (list of MRLs)
        """

        mrls = []
        parsed = urllib.parse.urlparse(urllib.parse.unquote(mrl))
        path = pathlib.Path(parsed.path)

        if parsed.scheme != 'file' or not path.exists():
            _logger.debug(f"Invalid M3U File URL: {mrl}")
            return []

        with path.open(encoding='utf-8') as fd:
            ix=0
            for line in fd:
                # Strip off whitespace including the newline character
                line = line.strip()

                # Get the actual filename
                if line.startswith('#'):
                    continue
                elif line.startswith('/'):
                    filename = pathlib.Path(line)
                elif line.startswith('./'):
                    filename = path.parent/line[2:]
                else:
                    filename = path.parent/line

                mrls.append(filename.as_uri())

        return mrls

    def dropMimeData(self, data, action, row, column, parent):
        try:
            if len(self.view) == 0:
                _sig_play_after = True
            else:
                _sig_play_after = False

            return self._dropMimeData(data, action, row, column, parent)

        finally:
            if _sig_play_after:
                self.app.signal.playAfter.emit(self.view.pointer(0))

    def _dropMimeData(self, data, action, row, column, parent):
        if 'text/uri-list' in data.formats():
            # NOTE:  QUrl tries to be helpful by default, prett-printing urls.
            #        ... this produces a questionably valid url
            #        ... this breaks library lookups by mrl
            #        ... and the url will normally never be looked at by a human!
            #        Solution:  Always decode the url using the 'FullyEncoded' formatting option
            mrls = [_.url(_.FullyEncoded) for _ in data.urls()]
            m3u = [_ for _ in mrls if _.endswith('.m3u') and _.startswith('file:')]

            if len(m3u) > 0 and len(m3u) != len(mrls):  # pragma: no cover
                raise NotImplementedError("Mixed m3u and file mime drops are not yet implemented!")

            elif len(m3u) > 0:
                to_add = [mrl for m3ufile in m3u for mrl in self._parseM3U(m3ufile)]
                self.select_add(to_add)
                return True

            elif action in (Qt.DropAction.CopyAction, Qt.DropAction.MoveAction):
                self.select_add(mrls)
                return True
        return False
