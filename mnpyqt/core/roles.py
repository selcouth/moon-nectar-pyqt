from enum         import Enum, unique
from PyQt6.QtCore import Qt

@unique
class UserRoles(Enum):
    AlbumSortByAlbum  = Qt.ItemDataRole.UserRole + 1
    AlbumSortByArtist = Qt.ItemDataRole.UserRole + 2
    AlbumSortByTrack  = Qt.ItemDataRole.UserRole + 3


