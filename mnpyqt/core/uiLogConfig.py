import logging

from PyQt6               import QtCore
from PyQt6               import QtWidgets
from PyQt6.QtCore        import Qt

from mnectar             import logConfig
from .                   import Dockable

import Registry.UI.PyQt

class QTextEditLogHandler(logging.Handler):
    def __init__(self, widget):
        super().__init__()
        self.widget = widget

    def emit(self, record):
        msg = self.format(record)
        self.widget.appendHtml(msg)

class QLogView(QtWidgets.QWidget, Dockable, Registry.Plugin,
        registry    = Registry.UI.PyQt.Docked,
        menu        = 'View',
        menu_name   = 'Log Window',
        location    = Qt.DockWidgetArea.BottomDockWidgetArea):

    LOG_FMT_LONG = "<b>%(asctime)s | %(levelname)-8s | %(name)-15s @ %(lineno)4d |</b> %(message)s"

    def __init__(self, *arg, **kw):
        super().__init__(*arg, **kw)
        self.config_ui()

    def config_ui(self):
        self.layout = QtWidgets.QVBoxLayout(self)
        self.logwin = QtWidgets.QPlainTextEdit(parent=self)
        self.layout.addWidget(self.logwin)

        self.handler = QTextEditLogHandler(self.logwin)
        self.formatter = logConfig.MultilineFormatter(self.LOG_FMT_LONG)
        self.handler.setFormatter(self.formatter)
        logging.getLogger(__package__.split('.')[0]).addHandler(self.handler)
