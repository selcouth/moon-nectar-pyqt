import logging
from PyQt6 import QtCore

import mnectar.logConfig

# Log handler which can be connected to QtSignals
class QLogSignalHandler(QtCore.QObject, logging.Handler):
    sigLogMessage = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        QtCore.QObject.__init__(self, parent)
        Hlogging.andler.__init__(self)

    def emit(self, record):
        self.sigLogMessage.emit(self.format(record))

