from qtconsole.rich_jupyter_widget import RichJupyterWidget
from qtconsole.inprocess import QtInProcessKernelManager

from PyQt6               import QtCore
from PyQt6               import QtWidgets
from PyQt6.QtCore        import Qt
from ..core              import Dockable

import mnectar

import Registry.UI.PyQt

class IPyConsole(QtWidgets.QWidget, Dockable, Registry.Plugin,
        registry    = Registry.UI.PyQt.Docked,
        menu        = 'View',
        menu_name   = 'IPython Console',
        menu_key    = "Ctrl+Shift+P",
        location    = Qt.DockWidgetArea.RightDockWidgetArea):

    def __init__(self, *arg, **kw):
        super().__init__(*arg, **kw)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.console = IPyConsoleWidget(
            customBanner="""app     - Moon Nectar Application Instance
mnectar - Moon Nectar module
"""
        )
        self.layout.addWidget(self.console)

        self.console.push_vars({
            'app':     self.app,
            'mnectar': mnectar,
        })

        self.app.ui.uiapp.aboutToQuit.connect(self.on_quit)
        self.console.exit_requested.connect(self.on_exit_requested)

        self.app.ui.console_var = self.console_var

    def console_var(self, **variables):
        """
        Make the specified variables available on the console with the given names.

        This is designed for debug use only!
        """
        self.console.push_vars(variables)

    def on_quit(self):
        self.console.kernel_client.stop_channels()
        self.console.kernel_manager.shutdown_kernel()

    def on_exit_requested(self):
        self._dock_show_action.qaction.trigger()


class IPyConsoleWidget(RichJupyterWidget):
    def __init__(self, customBanner=None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if customBanner is not None:
            self.banner = customBanner

        self.font_size = 6
        self.kernel_manager = kernel_manager = QtInProcessKernelManager()
        kernel_manager.start_kernel()
        kernel_manager.kernel.shell.banner1 = ""
        kernel_manager.kernel.gui = 'qt'
        self.kernel_client = kernel_client = self._kernel_manager.client()

        kernel_client.start_channels()

        def stop():
            kernel_client.stop_channels()
            kernel_manager.shutdown_kernel()

    def push_vars(self, variableDict):
        """
        Given a dictionary containing name / value pairs, push those variables
        to the Jupyter console widget
        """
        self.kernel_manager.kernel.shell.push(variableDict)

    def clear(self):
        """
        Clears the terminal
        """
        self._control.clear()

        # self.kernel_manager

    def print_text(self, text):
        """
        Prints some plain text to the console
        """
        self._append_plain_text(text)

    def execute_command(self, command):
        """
        Execute a command in the frame of the console widget
        """
        self._execute(command, False)


