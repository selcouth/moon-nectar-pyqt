import logging

from PyQt6.QtCore         import Qt
from PyQt6                import QtCore
from PyQt6                import QtWidgets
from mnectar.action       import Action
from mnectar.config       import Setting
from ..core               import Dockable
from ..core               import QPlaylistModel
from ..core               import QPlaylistView
from ..core               import QPlaylistFilterSortProxy
from ..core               import QActionable
from mnectar.library.view import Changed, Selected

import Registry.UI.PyQt

_logger = logging.getLogger("mnectar."+__name__)


class Queue(QPlaylistView, Dockable, QActionable, Registry.Plugin,
            registry   = Registry.UI.PyQt.Docked,
            menu       = 'View',
            menu_name  = 'Queue',
            menu_key   = "Ctrl+L",
            location   = Qt.DockWidgetArea.BottomDockWidgetArea,
            can_drag   = True,
            can_drop   = True,
            can_delete = True):

    delete_after_play = Setting(default                          = True)
    resume_previous   = Setting(default                          = True)
    force_noloop      = Setting('playback.force_noloop', default = False)

    act_queue_selected    = Action("Playback", "Queue", "Queue: Add Selected",
                                   "Ctrl+Return")
    act_queue_clear       = Action("Playback", "Queue", "Queue: Clear",
                                   "Ctrl+Shift+C")
    act_delete_selected   = Action("", "", "Queue: Delete Selected Tracks",
                                   "Backspace")
    act_delete_after_play = Action("", "", "Delete After Playback",
                                   checkable = True,
                                   setting = delete_after_play)
    act_resume_previous   = Action("", "", "Resume Previous Playlist on Empty Queue",
                                   checkable = True,
                                   setting = resume_previous)

    _to_delete = None
    _play_on_empty = None

    @act_queue_selected.triggered
    def on_queue_selected(self):
        self._proxy.select_add((_.mrl for _ in self.app.ui.selected))

    @act_queue_clear.triggered
    def on_queue_clear(self):
        self._proxy.select(self._proxy.SELECT_NONE)

    def __init__(self, *arg, app=None, **kw):
        self.app = app
        super().__init__(*arg, app=app, **kw)

    def config_ui(self):
        super().config_ui()
        self._model = QPlaylistModel(self.app.ui.library_view, app=self.app)
        self._proxy = QPlaylistFilterSortProxy(parent=self)
        self._proxy.setSourceModel(self._model, selected=self._proxy.SELECT_NONE)
        self._proxy.set_selectable(True)

        self.setName('Queue')
        self.setModel(self._proxy)

        self.app.signal.playing    .connect(self.on_playing)
        self.app.signal.playlistEnd.connect(self.on_playlist_end)
        self.app.signal.playEnd    .connect(self.on_play_end)

        self.addAction(self.act_delete_selected.qaction)

    def on_play_end(self, pointer):
        if pointer.view == self._proxy.view and self.delete_after_play:
            if pointer.next.valid:
                self.app.signal.playAfter.emit(pointer.next)

            if pointer.mrl in self._proxy.view:
                self._proxy.select_remove([pointer.mrl])

    def on_playlist_end(self, pointer):
        if self.resume_previous and self._play_on_empty and pointer.view == self._proxy.view and len(self._proxy.view) == 0:
            self.app.signal.playNew.emit(self._play_on_empty)

    def on_playing(self, pointer, length):
        # If the queue is not empty and not playing
        # ... mark the queue as next up to play
        if pointer.view != self._proxy.view and len(self._proxy.view) > 0:
            self.app.signal.playAfter.emit(self._proxy.view.pointer(0))

        # If another view is playing, save the pointer
        # ... so that when the queue empties, the previous playlist can be resumed
        if pointer.view != self._proxy.view:
            self._play_on_empty = pointer

        # Force non-looping playlist if deleting tracks after playback
        # ... this prevents bad behavior combining loop & delete-after-play
        # ... where the previous playlist is not properly resumed
        # ... because the backend plays a file that is about to be deleted
        if pointer.view == self._proxy.view and self.delete_after_play:
            self.force_noloop = True
        else:
            self.force_noloop = False

        # Change the border style if the queue is playing
        if pointer.view == self._proxy.view:
            self.style().unpolish(self)
            self.style().polish(self)
            self.update()
        else:
            self.style().unpolish(self)
            self.style().polish(self)
            self.update()

    def get_context_menu_actions(self, menu):
        super().get_context_menu_actions(menu)

        menu.addAction(self.act_queue_clear.qaction)
        menu.addAction(self.act_delete_selected.qaction)
        menu.addAction(self.act_delete_after_play.qaction)
        menu.addAction(self.act_resume_previous.qaction)

    @act_delete_selected.triggered
    def on_delete(self):
        self._proxy.select_remove(self._proxy.get_mrls(self.selectedIndexes()))

