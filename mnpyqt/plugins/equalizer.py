import logging

_logger = logging.getLogger('mnectar.'+__name__)

import functools

from PyQt6.QtCore                     import Qt
from PyQt6                            import QtCore
from PyQt6                            import QtGui
from PyQt6                            import QtWidgets

from mnectar.config                   import Setting
from mnectar.registry                 import Registry, Plugin, PluginSetting

from ..core                           import Dockable


class EqualizerDock(QtWidgets.QWidget, Plugin, Dockable,
                    registry  = Registry.UI.PyQt.Docked,
                    menu      = 'View',
                    menu_name = 'Equalizer',
                    menu_key  = 'Ctrl+E',
                    location  = Qt.DockWidgetArea.BottomDockWidgetArea):

    def __init__(self, *arg, **kw):
        super().__init__(*arg, **kw)

        self.config_ui()

    def config_ui(self):
        self.base_layout = QtWidgets.QVBoxLayout(self)
        self.presets = QtWidgets.QComboBox(self)
        self.presets.addItems(self.app.state.backend.equalizer.presets)
        self.base_layout.addWidget(self.presets)
        self.band_box = QtWidgets.QWidget(self)
        self.band_layout = QtWidgets.QGridLayout(self.band_box)
        self.band_layout.setSpacing(0)
        self.base_layout.addWidget(self.band_box)

        self.presets.currentTextChanged.connect(self.onPresetSelect)
        self.preamp_text = QtWidgets.QLabel("Preamp", self.band_box)
        self.preamp = QtWidgets.QSlider(Qt.Orientation.Vertical, self.band_box)
        self.preamp_value = QtWidgets.QLabel("0.0 dB", self.band_box)

        self.band_layout.addWidget(self.preamp, 0, 0, alignment=Qt.AlignmentFlag.AlignHCenter)
        self.band_layout.addWidget(self.preamp_text, 1, 0, alignment=Qt.AlignmentFlag.AlignHCenter)
        self.band_layout.addWidget(self.preamp_value, 2, 0, alignment=Qt.AlignmentFlag.AlignHCenter)

        self.preamp.setMaximum(int(self.app.state.backend.equalizer.max*10))
        self.preamp.setMinimum(int(self.app.state.backend.equalizer.min*10))
        self.preamp.setTracking(True)
        self.preamp.setTickInterval(10*5)
        self.preamp.setTickPosition(self.preamp.TickPosition.TicksLeft)

        self.preamp.valueChanged.connect(self.onPreampChanged)

        self.band_widgets = []
        self.band_text_widgets = []
        self.band_value_widgets = []
        for band in self.app.state.backend.equalizer.bands:
            if band < 1000:
                band_text = f"{int(band):d} Hz"
            else:
                band_text = f"{int(band/1000):d} kHz"

            self.band_text_widgets.append(QtWidgets.QLabel(band_text, self.band_box))
            self.band_widgets.append(QtWidgets.QSlider(Qt.Orientation.Vertical, self.band_box))
            self.band_value_widgets.append(QtWidgets.QLabel("0.0 db", self.band_box))

            self.band_layout.addWidget(self.band_widgets[-1], 0, len(self.band_widgets), alignment=Qt.AlignmentFlag.AlignHCenter)
            self.band_layout.addWidget(self.band_text_widgets[-1], 1, len(self.band_widgets), alignment=Qt.AlignmentFlag.AlignHCenter)
            self.band_layout.addWidget(self.band_value_widgets[-1], 2, len(self.band_widgets), alignment=Qt.AlignmentFlag.AlignHCenter)

            self.band_widgets[-1].setMaximum(int(self.app.state.backend.equalizer.max*10))
            self.band_widgets[-1].setMinimum(int(self.app.state.backend.equalizer.min*10))
            self.band_widgets[-1].setTracking(True)

            self.band_widgets[-1].valueChanged.connect(
                functools.partial(
                    self.onEqChanged,
                    widget=self.band_widgets[-1],
                    vwidget=self.band_value_widgets[-1],
                )
            )

        self.app.signal.equalizerChanged.connect(self.onEqualizerChanged)

        self.onEqualizerChanged()

    def onPresetSelect(self, name):
        self.app.signal.loadPreset.emit(name)

    def onEqualizerChanged(self):
        self.preamp.blockSignals(True)
        self.preamp.setValue(int(self.app.state.backend.equalizer.preamp * 10))
        self.preamp.blockSignals(False)
        self.preamp_value.setText(f"{self.app.state.backend.equalizer.preamp:.1f} dB")
        for ix in range(self.app.state.backend.equalizer.count):
            self.band_widgets[ix].blockSignals(True)
            self.band_widgets[ix].setValue(int(self.app.state.backend.equalizer.values[ix] * 10))
            self.band_widgets[ix].blockSignals(False)
            self.band_value_widgets[ix].setText(f"{self.app.state.backend.equalizer.values[ix]:.1f} dB")

    def onPreampChanged(self, value):
        self.preamp_value.setText(f"{value/10:.1f} dB")
        if not self.preamp.isSliderDown():
            self.app.signal.setEqPreamp.emit(value / 10)

    def onEqChanged(self, value, widget, vwidget):
        vwidget.setText(f"{value/10:.1f} dB")
        if not widget.isSliderDown():
            for ix in range(len(self.band_widgets)):
                self.app.signal.setEqValues.emit([w.value() / 10 for w in self.band_widgets])
