import pathlib
import setuptools
import distutils
import os
import subprocess
import re

from distutils.command.build import build as distutils_build

entry_points = [
    "Registry.UI.GuiPyQt                                                 = mnpyqt.core.uipyqt:GuiPyQt",
    "Registry.UI.PyQt                                                    = mnpyqt.core.__init__:PyQt",
    "Registry.UI.PyQt.Browsers                                           = mnpyqt.core.__init__:Browsers",
    "Registry.UI.PyQt.Browsers.DefaultBrowser                            = mnpyqt.core.browser:DefaultBrowser",
    "Registry.UI.PyQt.Docked                                             = mnpyqt.core.__init__:Docked",
    "Registry.UI.PyQt.Docked.QLogView                                    = mnpyqt.core.uiLogConfig:QLogView",
    "Registry.UI.PyQt.Menu                                               = mnpyqt.core.__init__:Menu",
    "Registry.UI.PyQt.Docked.Equalizer                                   = mnpyqt.plugins.equalizer:EqualizerDock",
    "Registry.UI.PyQt.Docked.HashtagManager                              = mnpyqt.plugins.hashtag.manager:HashtagManager",
    "Registry.UI.PyQt.Docked.IPyConsole                                  = mnpyqt.plugins.ipyconsole:IPyConsole",
# DEM 2021/01/08: PyConsole is not yet working with Qt6
#    "Registry.UI.PyQt.Docked.PyConsole                                   = mnpyqt.plugins.console:PyConsole",
    "Registry.UI.PyQt.Docked.Queue                                       = mnpyqt.plugins.queue:Queue",
    "Registry.UI.PyQt.Menu.OrderMenu                                     = mnpyqt.plugins.order:OrderMenu",
]

# Get the app version
# ... By switching to the package directory, the version file becomes a local import
# ... This avoids problems with missing dependencies
appvars = {}
with open("mnpyqt/vars.py") as fp:
    exec(fp.read(), appvars)

class BuildPyQt(setuptools.Command):
    description = "Compile PyQt UI files"
    user_options = [
        ('pyuic=',      None, 'pyuic executable'),
        ('pyuic-opts=', None, 'pyuic extra options'),
        ('packages=',   None, 'List of comma separated packages in which to recursively find .ui files'),
    ]

    def initialize_options(self):
        self.packages   = []
        self.pyuic      = 'pyuic6'
        self.pyuic_opts = []

    def finalize_options(self):
        if isinstance(self.packages, str):
            self.packages = [_.strip() for _ in self.packages.split(',')]

    def run(self):
        for package in self.packages:
            path = pathlib.Path(package)

            if self.pyuic:
                uifiles = path.glob("**/*.ui")
                for uifile in uifiles:
                    compiled = f"{str(uifile).replace('.ui','_UI.py')}"
                    command = [self.pyuic, str(uifile), *self.pyuic_opts, '-o', compiled]
                    self.announce(' '.join(command), level=distutils.log.INFO)
                    ret = subprocess.call(command)
                    if ret != 0:
                        self.announce(f'error compiling .ui file: {uifile}', level=distutils.log.ERROR)


class BuildAddPyQt(distutils_build):
    sub_commands = [('build_qt', None)] + distutils_build.sub_commands


module_packages = setuptools.find_packages()

def read_text(filename: str):
    return open(filename).read()

setuptools.setup(
    name                 = "mnpyqt",
    packages             = module_packages,
    use_scm_version      = True,
    license              = read_text("LICENSE.txt"),
    description          = "Moon Nectar Media Player PyQt GUI",
    author               = "David Morris",
    author_email         = "othalan@othalan.net",
    url                  = "https://gitlab.com/othalan/moon-nectar-pyqt",
    include_package_data = True,
    zip_safe             = False,
    python_requires      = '>=3.8',
    entry_points         = {
        "gui_scripts": [
            f"MoonNectar = mnectar.appinit:run_app",
        ],
        "mnectar.plugins": entry_points,
    },
    cmdclass    = {"build": BuildAddPyQt, "build_qt": BuildPyQt},
    options     = {"build_qt": {"packages": module_packages}},
    classifiers = [
        # complete classifier list: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
        "Operating System :: MacOS :: MacOS X",
        "Natural Language :: English",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.8",
        "Topic :: Multimedia :: Sound/Audio :: Players",
    ],
    setup_requires = [
        'setuptools_scm'
    ],
    install_requires = [
        "mnectar>=0.9.0",
        "pyqt6",
        "PluginRegistry>=0.3.0",
        "setuptools",
    ],
    extras_require = {
        "test": [
            "coveralls",
            "pytest",
            "pytest-clarity",
            "pytest-cov",
            "pytest-mock",
            "pytest-qt",
            "pytest-sugar",
            "pyfakefs",
        ],
        "console": ["pyqtconsole", "qtconsole"],
    },
)
