# Developer Installation:

```sh
pip install -e .
```

## Run using this command:

```sh
open --stdout /tmp/MN_stdout.txt --stderr /tmp/MN_stderr.txt --env PYENV_VERSION=$(pyenv version-name) bin/Moon\ Nectar\ Dev.app
```

